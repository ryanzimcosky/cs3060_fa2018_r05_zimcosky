# CS3060_FA2018_R05_ZIMCOSKY

## Running Tasks

### Task 1

```bash
| ?- ['Task_01.pl'].
compiling /Users/MisterZee/CS3060/cs3060_fa2018_r05_zimcosky/Task_01.pl for byte code...
/Users/MisterZee/CS3060/cs3060_fa2018_r05_zimcosky/Task_01.pl compiled, 7 lines read - 902 bytes written, 9 ms

(1 ms) yes
| ?- in(katarina,olga).

true ?

yes
| ?- in(katarina,irna).

true ?

yes
| ?- halt.
```

### Task 2

```bash
| ?- ['Task_02.pl'].
compiling /Users/MisterZee/CS3060/cs3060_fa2018_r05_zimcosky/Task_02.pl for byte code...
/Users/MisterZee/CS3060/cs3060_fa2018_r05_zimcosky/Task_02.pl compiled, 13 lines read - 1492 bytes written, 5 ms

(1 ms) yes
| ?- listtran([],[])
.

yes
| ?- listtran([eins, nuen, zwei],X).

X = [one,nine,two]

yes
| ?- halt.
```

## Reflection Questions

1. What was the major question or topic addressed in the classroom?
   - How to use lists in prolog.

2. Summarize what you learned during this week of class. Note any particular features, concepts, code,or terms that you believe are important.
   - I learned how to denote lists in prolog as the head and tail elements of that list, then likewise traverse through that list by recursively calling the rule using the tails of the list.

   ```prolog
   listtran([H|T],[A|B]) :- tran(H, A),
           listtran(T, B).
   ```

3. What did you learn about the current language that is different from what you have previously experienced? Do you think it is a good difference or bad difference?
   - Rather than using loops, to achieve the same effect in prolog, you must use recursion.

4. Do you learn answers to any of the 5 fundamental questions we are asking about each programming language this week?

   1. What is the typing model?
      - Strongly statically typed

   2. What is the programming model?
      - Logical declaritive language

   3. Is the language Interpreted or Compiled?
      - compiled

   4. What are the decision constructs & core data structures?
      - Facts, rules, querys and knowlege bases

5. What  was  the  most  difficult  aspect  of  what  you  learned  in  class?  Why  was  this  the  most  difficult aspect? What can you do to make it easier for you?
   - The most difficult aspect is the change in my programming procedure between veiwing my code as a series of steps to a solution, and veiwing it as facts and rules. I need to change the way I think about programming in order to make quality knowlege bases in prolog.

6. What mistakes did your group make in solving your task?  What mistakes did you note the opposing group made?
   - In class, our group started with Task 1, and we seemed to have the correct solution. Both my group and the other group struggled with task 2. We were trying to find a wat to make a head and a tail for each list variable in the guts of our rule instead of just denoting the two variables as a head with a tail in the rule's declaration.  