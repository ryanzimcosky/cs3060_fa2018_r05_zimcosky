%% Ryan Zimcosky
%% Knowlege base telling which dolls are in others.

% directlyIn(X,Y) implies that Y is directly in X.
directlyIn(natasha, irina).
directlyIn(olga, natasha).
directlyIn(katarina, olga).

% base case.
in(X,Y) :- directlyIn(X,Y).

% Y is in X if Y is in something that is directly in X.
in(X,Y) :- directlyIn(X, Z),
    in(Z,Y).